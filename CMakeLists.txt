cmake_minimum_required(VERSION 3.8)
project(TGSamples)


# ===============================================
# Global settings

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set_property(GLOBAL PROPERTY USE_FOLDERS ON)

# for now, typed-graphics is too alpha to be default
option(TG_TESTS "Build typed-geometry tests" ON)
option(TG_EIGEN_TESTS "Build typed-geometry tests that require eigen" OFF)
option(TG_SAMPLES "Build typed-geometry samples" ON)
option(TG_COVERAGE "Generate test coverage information" OFF)
option(TG_IMPLEMENTATION_REPORT "Test-case that generates implementation coverage" OFF)

# ==============================================================================
# Set bin dir
if(MSVC)
    set(BIN_DIR ${CMAKE_SOURCE_DIR}/bin)
elseif(CMAKE_BUILD_TYPE STREQUAL "")
    set(BIN_DIR ${CMAKE_SOURCE_DIR}/bin/Default)
else()
    set(BIN_DIR ${CMAKE_SOURCE_DIR}/bin/${CMAKE_BUILD_TYPE})
endif()
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${BIN_DIR})
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_RELEASE ${BIN_DIR})
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_DEBUG ${BIN_DIR})

# ===============================================
# Dependencies

# ctracer tracing library
add_subdirectory(extern/ctracer)

# math library
add_subdirectory(extern/typed-geometry)

# mesh library
add_subdirectory(extern/polymesh)

# GLM math (due to compat tests)
add_library(glm INTERFACE)
target_include_directories(glm INTERFACE extern/glm)
target_compile_definitions(glm INTERFACE
    GLM_FORCE_CXX11
    GLM_ENABLE_EXPERIMENTAL
    GLM_FORCE_CTOR_INIT # 0.9.9 breaks a lot otherwise
    GLM_FORCE_SILENT_WARNINGS
)
if (CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
    target_compile_options(glm INTERFACE
        -Wno-class-memaccess
        -Wno-conversion
        )
endif()

if (TG_SAMPLES)
    # Add GLFW lib (with disabled spam)
    option(GLFW_BUILD_EXAMPLES "" OFF)
    option(GLFW_BUILD_TESTS "" OFF)
    option(GLFW_BUILD_DOCS "" OFF)
    option(GLFW_INSTALL "" OFF)
    add_subdirectory(extern/glfw)

    # glow
    add_subdirectory(extern/glow)

    # imgui-lean
    add_subdirectory(extern/imgui-lean)

    # glow extras
    add_subdirectory(extern/glow-extras)
endif()

if(TG_EIGEN_TESTS)
    find_package (Eigen3 3.3 REQUIRED NO_MODULE)
endif()

# ===============================================
# Compile flags

if (MSVC)
    set(SAMPLE_FLAGS
        /MP
    )
    set(TEST_FLAGS
        ${SAMPLE_FLAGS}
    )
else()
    set(SAMPLE_FLAGS
        -Wall
        -Werror
        -march=native
    )
    set(TEST_FLAGS
        ${SAMPLE_FLAGS}
        -Wconversion
        -Wextra
        -Wpedantic
    )
    if (CMAKE_CXX_COMPILER_ID MATCHES "Clang")
        set(TEST_FLAGS
            ${TEST_FLAGS}
            -Wno-nested-anon-types
            -Wno-gnu-anonymous-struct
        )
    endif()
endif()

if (TG_IMPLEMENTATION_REPORT)
    set(TEST_FLAGS ${TEST_FLAGS} -DTG_IMPLEMENTATION_REPORT)
endif()

set(TEST_LINK_FLAGS "")

if (TG_COVERAGE)
    if (CMAKE_CXX_COMPILER_ID MATCHES "Clang")
        set(TEST_FLAGS ${TEST_FLAGS} -fprofile-instr-generate -fcoverage-mapping)
    elseif(CMAKE_CXX_COMPILER_ID MATCHES "GNU")
        set(TEST_FLAGS ${TEST_FLAGS} --coverage -fprofile-arcs -ftest-coverage)
        set(TEST_LINK_FLAGS ${TEST_LINK_FLAGS} -lgcov --coverage)
    else()
        message(FATAL_ERROR "Coverage currently only support for clang and gcc")
    endif()
endif()

# ===============================================
# Enumerate samples

if (TG_SAMPLES)
    file(GLOB SAMPLE_DIRS LIST_DIRECTORIES true "samples/*")

    foreach(dir ${SAMPLE_DIRS})
        file(RELATIVE_PATH sample "${CMAKE_CURRENT_SOURCE_DIR}/samples/" ${dir})
        message(STATUS "Adding sample ${sample}")

        file(GLOB_RECURSE SOURCES
            "samples/${sample}/*.cc"
            "samples/${sample}/*.hh"
        )

        source_group(TREE ${CMAKE_CURRENT_SOURCE_DIR} FILES ${SOURCES})

        add_executable(sample-${sample} ${SOURCES})

        target_link_libraries(sample-${sample} PUBLIC
            typed-geometry
            polymesh
            ctracer
            glow
            glow-extras
        )

        target_include_directories(sample-${sample} PUBLIC "samples/${sample}")

        target_compile_options(sample-${sample} PUBLIC ${SAMPLE_FLAGS})

        if (NOT MSVC)
            target_compile_options(sample-${sample} PUBLIC -Wno-sign-conversion) # glm again ...
        endif()

    endforeach()
endif()


# ===============================================
# Build tests

if (TG_TESTS)
    file(GLOB_RECURSE SOURCES
        "tests/*.cc"
        "tests/*.hh"
    )

    if(NOT TG_ENABLE_FIXED_INT)
        list(FILTER SOURCES EXCLUDE REGEX ".*fixed_int.cc")
        list(FILTER SOURCES EXCLUDE REGEX ".*fixed_uint.cc")
    endif()

    source_group(TREE ${CMAKE_CURRENT_SOURCE_DIR} FILES ${SOURCES})

    add_executable(tg-tests ${SOURCES})

    target_link_libraries(tg-tests PUBLIC
        typed-geometry
        polymesh
        ctracer
        glm
        -lgmp
        -lgmpxx
        ${TEST_LINK_FLAGS}
    )

    if(TG_EIGEN_TESTS)
        target_compile_definitions(tg-tests PUBLIC TG_EIGEN_TESTS 1)
        target_link_libraries (tg-tests PUBLIC Eigen3::Eigen)
    endif()

    target_include_directories(tg-tests PUBLIC "tests")

    target_compile_options(tg-tests PUBLIC ${TEST_FLAGS})
endif()


# ===============================================
# Folder grouping

foreach(TARGET_NAME
    glfw
    ctracer
    polymesh
    typed-geometry
    glad
    glow
    glow-extras
    imgui
    lodepng
    stb
)
    if (TARGET ${TARGET_NAME})
        set_property(TARGET ${TARGET_NAME} PROPERTY FOLDER "Extern")
    endif()
endforeach()

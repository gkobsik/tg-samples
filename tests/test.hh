#pragma once

#include <doctest.hh>

#include <ctracer/trace.hh>

#include <typed-geometry/feature/basic.hh>
#include <typed-geometry/feature/objects.hh>
#include <typed-geometry/feature/random.hh>
#include <typed-geometry/functions/std/io.hh>
#include <typed-geometry/tg-lean.hh>

// TG_FUZZ_TEST(Case, Name, MaxIterations = 1000, MaxCycles = 50 * 1000 * 1000)
#define TG_FUZZ_TEST(test_case_name, test_name) TG_FUZZ_TEST_MAX_ITS_MAX_CYCLES(test_case_name, test_name, 1000, 50 * 1000 * 1000)
#define TG_FUZZ_TEST_MAX_ITS(test_case_name, test_name, max_its) TG_FUZZ_TEST_MAX_ITS_MAX_CYCLES(test_case_name, test_name, max_its, 50 * 1000 * 1000)
#define TG_FUZZ_TEST_MAX_ITS_MAX_CYCLES(test_case_name, test_name, max_its, max_cycles) \
    void tg_fuzz_test_##test_case_name##_##test_name(tg::rng& rng);                     \
    TEST_CASE(#test_case_name "." #test_name)                                           \
    {                                                                                   \
        TRACE("Fuzz Test " #test_case_name #test_name);                                 \
        auto its = max_its;                                                             \
        auto cycles_end = ct::current_cycles() + max_cycles;                            \
        tg::rng rng;                                                                    \
        /*rng.seed(::testing::UnitTest::GetInstance()->random_seed());*/                \
        while (its > 0 && ct::current_cycles() < cycles_end)                            \
        {                                                                               \
            --its;                                                                      \
            tg_fuzz_test_##test_case_name##_##test_name(rng);                           \
        }                                                                               \
    }                                                                                   \
    void tg_fuzz_test_##test_case_name##_##test_name(tg::rng& rng)

inline doctest::Approx approx(double v) { return doctest::Approx(v); }

template <int D, class T, class ScalarT>
struct comp_approx
{
    T value;
    ScalarT eps;

    bool operator==(T const& rhs) const
    {
        for (auto i = 0; i < D; ++i)
            if (tg::abs(value[i] - rhs[i]) > eps)
                return false;
        return true;
    }
};
template <int R, int C, class T, class ScalarT>
struct mat_approx
{
    T value;
    ScalarT eps;

    bool operator==(T const& rhs) const
    {
        for (auto x = 0; x < C; ++x)
            for (auto y = 0; y < R; ++y)
                if (tg::abs(value[x][y] - rhs[x][y]) > eps)
                    return false;
        return true;
    }
};

template <class T>
struct angle_approx
{
    T value;
    T eps;

    bool operator==(T const& rhs) const { return abs(value - rhs) <= eps; }
};

template <int D, class T, class ScalarT>
doctest::String toString(comp_approx<D, T, ScalarT> const& in)
{
    auto r = doctest::String("approx(");
    for (auto i = 0; i < D; ++i)
    {
        if (i > 0)
            r += ", ";
        r += doctest::toString(in.value[i]);
    }
    r += ")";
    return r;
}
template <int R, int C, class T, class ScalarT>
doctest::String toString(mat_approx<R, C, T, ScalarT> const& in)
{
    auto r = doctest::String("approx(");
    for (auto x = 0; x < C; ++x)
    {
        if (x > 0)
            r += ", ";
        r += "[";
        for (auto y = 0; y < R; ++y)
        {
            if (y > 0)
                r += ", ";
            r += doctest::toString(in.value[x][y]);
        }
        r += "]";
    }
    r += ")";
    return r;
}
template <class T>
doctest::String toString(angle_approx<T> const& in)
{
    return doctest::String("approx(") + tg::to_string(in.value).c_str() + ")";
}

template <int D, class T, class ScalarT>
bool operator==(T const& v, comp_approx<D, T, ScalarT> const& a)
{
    return a == v;
}
template <int R, int C, class T, class ScalarT>
bool operator==(T const& v, mat_approx<R, C, T, ScalarT> const& a)
{
    return a == v;
}
template <class T>
bool operator==(T const& v, angle_approx<T> const& a)
{
    return a == v;
}

template <class T>
angle_approx<tg::angle_t<T>> approx(tg::angle_t<T> const& a, tg::angle_t<T> eps = tg::degree(100 * tg::epsilon<T>))
{
    return {a, eps};
}

template <int R, int C, class T>
mat_approx<R, C, tg::mat<R, C, T>, T> approx(tg::mat<R, C, T> const& v, T eps = 100 * tg::epsilon<T>)
{
    return {v, eps};
}

#define MAKE_COMP_APPROX(type)                                                                      \
    template <int D, class T>                                                                       \
    comp_approx<D, tg::type<D, T>, T> approx(tg::type<D, T> const& v, T eps = 100 * tg::epsilon<T>) \
    {                                                                                               \
        return {v, eps};                                                                            \
    }                                                                                               \
    TG_FORCE_SEMICOLON

MAKE_COMP_APPROX(vec);
MAKE_COMP_APPROX(dir);
MAKE_COMP_APPROX(pos);
MAKE_COMP_APPROX(comp);
MAKE_COMP_APPROX(color);
MAKE_COMP_APPROX(size);

template <class T>
comp_approx<4, tg::quaternion<T>, T> approx(tg::quaternion<T> const& v, T eps = 100 * tg::epsilon<T>)
{
    return {v, eps};
}

template <int D, class T = float, class Rng>
tg::vec<D, T> random_vector(Rng& rng, T range = 5.f)
{
    tg::vec<D, T> v;
    for (auto x = 0; x < D; ++x)
        v[x] = uniform(rng, -range, range);
    return v;
}
template <int D, class T = float, class Rng>
tg::mat<D, D, T> random_matrix(Rng& rng, T range = 2.f)
{
    tg::mat<D, D, T> m;
    for (auto x = 0; x < D; ++x)
        for (auto y = 0; y < D; ++y)
            m[x][y] = uniform(rng, -range, range);
    return m;
}
template <int D, class T = float, class Rng>
tg::mat<D, D, T> random_invertible_matrix(Rng& rng, T range = 2.f)
{
    while (true)
    {
        auto m = random_matrix<D, T>(rng, range);
        if (tg::abs(determinant(m)) > T(0.1))
            return m;
    }
}

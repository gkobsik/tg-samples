#include "test.hh"

TG_FUZZ_TEST(TypedGeometry, Centroid)
{
    auto range1 = tg::aabb1(tg::pos1(-10), tg::pos1(10));
    auto range2 = tg::aabb2(tg::pos2(-10), tg::pos2(10));
    auto range3 = tg::aabb3(tg::pos3(-10), tg::pos3(10));

    {
        auto b = aabb_of(uniform(rng, range3), uniform(rng, range3));
        CHECK(contains(b, centroid(b)));
    }
    {
        auto b = aabb_of(uniform(rng, range2), uniform(rng, range2));
        CHECK(contains(b, centroid(b)));
    }
    {
        auto t = tg::triangle2(uniform(rng, range2), uniform(rng, range2), uniform(rng, range2));
        auto center = centroid(t);
        CHECK(contains(t, center));
        auto transl = uniform(rng, range2);

        t.pos0.x += transl.x;
        t.pos0.y += transl.y;
        t.pos1.x += transl.x;
        t.pos1.y += transl.y;
        t.pos2.x += transl.x;
        t.pos2.y += transl.y;

        center.x += transl.x;
        center.y += transl.y;

        CHECK(contains(t, center));
    }



    {
        // 3d sphere
        auto radius = uniform(rng, range1).x;
        auto sphere = tg::sphere3(uniform(rng, range3), radius);
        auto fullTurn = tg::pi<float>.radians() * 2;

        auto center = centroid(sphere);

        for (auto s = 0.0f; s < fullTurn; s += 0.01f)
        {
            for (auto t = 0.0f; t < fullTurn; t += 0.01f)
            {
                auto surfacePoint = sphere.center + tg::vec3(tg::f32(cos(s) * sin(t) * radius), tg::f32(sin(s) * sin(t) * radius), tg::f32(cos(t) * radius));

                CHECK(tg::distance_sqr(center, surfacePoint) == approx(pow(radius, 2)));
            }
        }
    }

}

#include "test.hh"

TG_FUZZ_TEST(TypedGeometry, Area)
{
    auto box1 = tg::aabb1(-1.0f, 1.0f);
    auto box2 = tg::aabb2(tg::pos2(-1.0f), tg::pos2(1.0f));
    auto box3 = tg::aabb3(tg::pos3(-1.0f), tg::pos3(1.0f));

    {
        // 2D
        tg::f32 x = tg::abs(uniform(rng, box1).x) * 9 + 1;
        tg::pos2 vertices[] = {tg::pos2(0, 0), tg::pos2(x, 0), tg::pos2(x, x)};

        // random 2d transformation
        auto alpha = tg::radians(tg::abs(uniform(rng, box1).x) * tg::pi_scalar<tg::f32>);
        auto rotation = tg::mat2::from_cols(tg::vec2(tg::cos(alpha), -sin(alpha)), tg::vec2(sin(alpha), cos(alpha)));
        tg::f32 randomScale = 0;
        auto max = 10.f;
        while (randomScale == 0)
            randomScale = max * uniform(rng, box1).x;
        auto scale = tg::mat2::from_cols(tg::vec2(randomScale, 0), tg::vec2(0, randomScale));
        auto translation = tg::translation(max * uniform(rng, box2) - tg::pos2::zero);

        // apply transformations on vertices
        for (tg::pos2& p : vertices)
        {
            p = translation * tg::pos2(rotation * scale * tg::vec2(p));
        }

        auto triangle = tg::triangle2(vertices[0], vertices[1], vertices[2]);

        // if triangle was scaled respect that
        x *= randomScale;
        const auto triangleArea = x * x / 2;
        {
            CHECK(triangleArea == approx(area(triangle)));
        }

        // definitely smaller than aabb
        CHECK(area(triangle) < area(aabb_of(triangle)));

        {
            // 2d equilateral triangle
            tg::f32 y = tg::sqrt(tg::pow(x, 2) - tg::pow(x / 2, 2));
            tg::pos2 vertices[] = {tg::pos2(0, 0), tg::pos2(x, 0), tg::pos2(x / 2, y)};

            // scale and translate vertices (rotation would make below equation invalid, as aabb is not rotated)
            for (auto& p : vertices)
            {
                p = translation * tg::pos2(scale * tg::vec2(p));
            }

            auto equiTriangle = tg::triangle2(vertices[0], vertices[1], vertices[2]);

            auto equiTriangleArea = area(equiTriangle);
            // the area of a nonrotated triangle is half the area of its aabb
            CHECK(equiTriangleArea * 2.0f == approx(area(aabb_of(equiTriangle))));
        }
    }

    {
        // 3D
        tg::f32 x = tg::abs(uniform(rng, box1)).x * 9 + 1;
        tg::pos3 vertices[] = {tg::pos3(0, 0, 0), tg::pos3(x, 0, 0), tg::pos3(x, x, 0)};

        auto alpha = tg::radians(tg::abs(uniform(rng, box1).x) * tg::pi_scalar<tg::f32>);
        auto rotation = rotation_x(alpha) * rotation_y(alpha) * rotation_z(alpha);

        tg::f32 randomScale = 0;
        auto max = 10.f;
        while (randomScale == 0)
            randomScale = max * uniform(rng, box1).x;

        auto rand_size = tg::size3(randomScale, randomScale, randomScale);
        auto scale = scaling(rand_size);

        auto translation = tg::translation(max * uniform(rng, box3) - tg::pos3::zero);

        for (auto& p : vertices)
        {
            tg::vec4 v = {p.x, p.y, p.z, 1};
            v = translation * rotation * scale * v;
            p = tg::pos3(v.x, v.y, v.z);
        }

        auto triangle = tg::triangle3(vertices[0], vertices[1], vertices[2]);

        x *= randomScale;
        const auto triangleArea = x * x / 2.0f;

        CHECK(triangleArea == approx(area(triangle)));
    }

    {
        auto r = tg::abs(uniform(rng, box1).x) * 10;
        auto h = tg::abs(uniform(rng, box1).x) * 10;
        auto c = tg::cylinder3({0, 0, 0}, {0, h, 0}, r);

        // surface area
        CHECK(area(c) == approx(h * 2 * r * tg::pi<tg::f32>.radians() + 2 * tg::pow2(r) * tg::pi<tg::f32>.radians()));
    }

    {
        auto r = uniform(rng, box1).x;
        auto d = tg::sphere2in3({0, 0, 0}, r, tg::dir3::pos_y);
        CHECK(area(d) == approx(tg::pi_scalar<tg::f32> * tg::pow2(r)));
    }

    {
        auto r = uniform(rng, box1).x;
        auto s = tg::sphere3({0, 0, 0}, r);
        CHECK(area(s) == approx(4 * tg::pi_scalar<tg::f32> * tg::pow2(r)));
    }

    {
        auto h = tg::abs(uniform(rng, box1).x) * 10;
        auto pyra = tg::pyramid<tg::box2in3>(tg::box2in3({0, 0, 0}, tg::mat2x3::identity), h);
        (void)pyra;

        // surface area
        // TODO: fixme
        // CHECK(area(pyra) == approx(tg::pow2(l) + 4 * l * sqrt(tg::pow2(h) + tg::pow2(l / 2.0)) / 2.0));
    }
}

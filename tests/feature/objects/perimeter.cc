#include "test.hh"

TG_FUZZ_TEST(TypedGeometry, Perimeter)
{
    // ball2, sphere2, disk2/3, circle2/3, rect2/3

    // positive
    auto box1 = tg::aabb1(0.1f, 10.0f);

    auto box2 = tg::aabb2(tg::pos2(-10.0f), tg::pos2(10.0f));
    auto box3 = tg::aabb3(tg::pos3(-10.0f), tg::pos3(10.0f));

    // random radius
    auto radius = tg::uniform(rng, box1).x;
    {
        auto center2 = tg::uniform(rng, box2);
        auto center3 = tg::uniform(rng, box3);

        auto ball = tg::sphere<2,float>(center2, radius);
        auto sphere = tg::sphere_boundary<2, float>(center2, radius);

        auto disk3 = tg::sphere2in3(center3, radius, tg::dir3::pos_y);
        auto circle3 = tg::sphere_boundary<2, float, 3>(center3, radius, tg::dir3::pos_y);

        // must all be the same
        auto pb = tg::perimeter(ball);
        auto ps = tg::perimeter(sphere);
        auto pd3 = tg::perimeter(disk3);
        auto pc3 = tg::perimeter(circle3);

        CHECK(pb == approx(ps));
        CHECK(pd3 == approx(pc3));
    }

    // known radius
    radius = 1.0f / (2.0f * tg::pi<tg::f32>.radians());
    {
        auto center2 = tg::uniform(rng, box2);
        auto center3 = tg::uniform(rng, box3);

        auto ball = tg::sphere<2,float>(center2, radius);
        auto sphere = tg::sphere_boundary<2, float>(center2, radius);

        auto disk3 = tg::sphere2in3(center3, radius, tg::dir3::pos_y);
        auto circle3 = tg::sphere_boundary<2, float, 3>(center3, radius, tg::dir3::pos_y);

        // must all be the same
        auto pb = tg::perimeter(ball);
        auto ps = tg::perimeter(sphere);
        auto pd3 = tg::perimeter(disk3);
        auto pc3 = tg::perimeter(circle3);

        CHECK(pb == approx(1.0f));
        CHECK(ps == approx(1.0f));
        CHECK(pd3 == approx(1.0f));
        CHECK(pc3 == approx(1.0f));
    }
}

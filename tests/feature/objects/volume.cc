#include "test.hh"

TG_FUZZ_TEST(TypedGeometry, Volume)
{
    {
        auto box3 = tg::aabb3(tg::pos3(-1.0f), tg::pos3(1.0f));
        auto box1 = tg::aabb1(-1.0f, 1.0f);

        auto c = uniform(rng, box3);
        auto r = tg::abs(uniform(rng, box1).x);
        auto sphere = tg::sphere3(c, r);
        auto ball = tg::sphere3(sphere);

        auto vb = volume(ball);
        CHECK(vb == approx(4.0f / 3.0f * tg::pi_scalar<tg::f32> * tg::pow3(r)));

        auto n = tg::uniform<tg::dir3>(rng);
        auto cylinder = tg::cylinder3(c, c + n * r * 2, r);
        auto vc = volume(cylinder);
        CHECK(vc == approx(3.0f / 2.0f * vb));

        // position and orientation is irrelevant
        auto cone = tg::cone3({tg::pos3::zero, r, tg::dir3::pos_y}, r);

        // volume of ball is equal to volume of cylinder - 2 * volume of cone
        CHECK(vb == approx(vc - 2 * volume(cone)));
    }
}

#include <test.hh>

#include <glm/glm.hpp>

static glm::vec3 glm_cast(tg::pos3 p) { return {p.x, p.y, p.z}; }
static glm::vec3 glm_cast(tg::vec3 v) { return {v.x, v.y, v.z}; }

TG_FUZZ_TEST(Glm, Interop)
{
    auto bb = tg::aabb3(-10, 10);

    // tg::pos -> glm::vec -> tg::pos
    {
        auto p = uniform(rng, bb);
        auto v = glm_cast(p);
        auto p2 = tg::pos3(v);
        CHECK(p == p2);
    }

    // tg::mat -> glm::mat -> tg::mat
    {
        tg::mat3 m;
        m[0] = uniform_vec(rng, bb);
        m[1] = uniform_vec(rng, bb);
        m[2] = uniform_vec(rng, bb);
        auto gm = glm::mat3(glm_cast(m[0]), glm_cast(m[1]), glm_cast(m[2]));
        auto m2 = tg::mat3(gm);
        CHECK(m == m2);
    }
}

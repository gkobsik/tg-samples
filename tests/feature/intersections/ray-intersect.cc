#include <test.hh>

TG_FUZZ_TEST(Ray, Intersect)
{
    auto bounds = tg::aabb3(-10, 10);

    // ray - plane
    {
        auto const r = tg::ray3(uniform(rng, bounds), tg::uniform<tg::dir3>(rng));
        auto const p = tg::plane(tg::uniform<tg::dir3>(rng), uniform(rng, bounds));

        auto t = intersection_parameter(r, p);

        if (t.has_value())
        {
            auto const ip = r[t.value()];

            CHECK(distance(ip, p) == approx(0).epsilon(1e-2f));
        }
    }

    // ray - tube
    {
        auto const r = tg::ray3(uniform(rng, bounds), tg::uniform<tg::dir3>(rng));
        auto const t = tg::cylinder_boundary_no_caps<3, float>(uniform(rng, bounds), uniform(rng, bounds), uniform(rng, 0.5f, 10.0f));

        auto is = intersection_parameter(r, t);
        for (auto i : is)
        {
            auto ip = r[i];

            CHECK(distance(ip, t) == approx(0).epsilon(1e-2f));
        }
    }

    // ray - disk
    {
        auto const r = tg::ray3(uniform(rng, bounds), tg::uniform<tg::dir3>(rng));
        auto const d = tg::sphere2in3(uniform(rng, bounds), uniform(rng, 0.5f, 10.0f), tg::uniform<tg::dir3>(rng));

        auto ip = intersection(r, d);

        if (ip.has_value())
            CHECK(distance(ip.value(), d) == approx(0).epsilon(1e-2f));
    }

    // ray - cylinder
    {
        auto const r = tg::ray3(uniform(rng, bounds), tg::uniform<tg::dir3>(rng));
        auto const c = tg::cylinder3(uniform(rng, bounds), uniform(rng, bounds), uniform(rng, 0.5f, 10.0f));
        auto const t = tg::cylinder_boundary_no_caps<3, float>(c.axis, c.radius);

        auto it = closest_intersection(r, t);
        if (it.has_value())
        {
            CHECK(distance(it.value(), t) == approx(0).epsilon(1e-2f));
            CHECK(distance(it.value(), c) == approx(0).epsilon(1e-2f));
        }

        auto ip = closest_intersection(r, c);

        if (ip.has_value())
            CHECK(distance(ip.value(), c) == approx(0).epsilon(1e-2f));
    }
}

TG_FUZZ_TEST(Intersect, LineLine2)
{
    auto bb = tg::aabb2(-10.f, 10.f);
    auto l0 = tg::line2::from_points(uniform(rng, bb), uniform(rng, bb));
    auto l1 = tg::line2::from_points(uniform(rng, bb), uniform(rng, bb));

    auto [t0, t1] = tg::intersection_parameters(l0, l1);
    CHECK(l0[t0] == approx(l1[t1], 0.01f));
}

TG_FUZZ_TEST(Intersect, SegSeg2)
{
    auto bb = tg::aabb2(-10.f, 10.f);
    auto s0 = tg::segment2(uniform(rng, bb), uniform(rng, bb));
    auto s1 = tg::segment2(uniform(rng, bb), uniform(rng, bb));

    auto ip = intersection(s0, s1);
    if (ip.has_value())
    {
        CHECK(distance(ip.value(), s0) < 0.01f);
        CHECK(distance(ip.value(), s1) < 0.01f);
    }
}

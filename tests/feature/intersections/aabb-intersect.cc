#include <test.hh>

TG_FUZZ_TEST(AABB, Intersect)
{
    auto const bounds = tg::aabb3(-10, 10);

    {
        auto const min_a = uniform(rng, bounds);
        auto const max_a = uniform(rng, tg::aabb3(min_a, bounds.max));

        auto const aabb_a = tg::aabb3(min_a, max_a);

        auto const inside = uniform(rng, aabb_a);
        auto const other = uniform(rng, bounds);

        const tg::pos3 min_b = {
            tg::min(inside.x, other.x),
            tg::min(inside.y, other.y),
            tg::min(inside.z, other.z),
        };

        const tg::pos3 max_b = {
            tg::max(inside.x, other.x),
            tg::max(inside.y, other.y),
            tg::max(inside.z, other.z),
        };

        auto const aabb_b = tg::aabb3(min_b, max_b);

        CHECK(tg::intersects(aabb_a, aabb_b));
    }
}


TG_FUZZ_TEST(AABB, NonIntersect)
{
    auto const bounds = tg::aabb3(-10, 10);

    {
        auto const min_a = uniform(rng, bounds);
        auto const max_a = uniform(rng, tg::aabb3(min_a, bounds.max));

        auto const aabb_a = tg::aabb3(min_a, max_a);

        auto min_b = uniform(rng, bounds);
        auto max_b = uniform(rng, tg::aabb3(min_b, bounds.max));

        // generate one separating axis
        auto const dim = uniform(rng, {0, 1, 2});
        auto const dir = uniform(rng, {true, false});
        if (dir)
        {
            min_b[dim] = uniform(rng, max_a[dim], bounds.max[dim]);
            max_b[dim] = uniform(rng, min_b[dim], bounds.max[dim]);
        }
        else
        {
            max_b[dim] = uniform(rng, bounds.min[dim], min_a[dim]);
            min_b[dim] = uniform(rng, bounds.min[dim], max_b[dim]);
        }

        auto const aabb_b = tg::aabb3(min_b, max_b);

        CHECK(!tg::intersects(aabb_a, aabb_b));
    }
}

TG_FUZZ_TEST(AABB, SamplingTest)
{
    auto const bounds = tg::aabb3(-10, 10);
    {
        auto const min_a = uniform(rng, bounds);
        auto const max_a = uniform(rng, tg::aabb3(min_a, bounds.max));

        auto const min_b = uniform(rng, bounds);
        auto const max_b = uniform(rng, tg::aabb3(min_b, bounds.max));

        auto const aabb_a = tg::aabb3(min_a, max_a);
        auto const aabb_b = tg::aabb3(min_b, max_b);

        auto const res = intersection(aabb_a, aabb_b);

        if (res.has_value())
        {
            auto const intsct = res.value();

            for (auto i = 0; i < 100; ++i)
            {
                auto const v = uniform(rng, bounds);

                if (contains(intsct, v))
                {
                    CHECK(contains(aabb_a, v));
                    CHECK(contains(aabb_b, v));
                }

                if (contains(aabb_a, v) && contains(aabb_b, v))
                {
                    CHECK(contains(intsct, v));
                }

                if (!contains(aabb_a, v))
                {
                    CHECK(!contains(intsct, v));
                }

                if (!contains(aabb_b, v))
                {
                    CHECK(!contains(intsct, v));
                }
            }
        }
        else
        {
            // no intersection
            for (auto i = 0; i < 50; ++i)
            {
                auto const v = uniform(rng, aabb_a);
                CHECK(!contains(aabb_b, v));
            }

            for (auto i = 0; i < 50; ++i)
            {
                auto const v = uniform(rng, aabb_b);
                CHECK(!contains(aabb_a, v));
            }
        }
    }
}

TG_FUZZ_TEST(AABB, SphereIntersection)
{
    auto const bounds = tg::aabb3(-10, 10);

    auto const bb = aabb_of(uniform(rng, bounds), uniform(rng, bounds));
    auto const s = tg::sphere3(uniform(rng, bounds), uniform(rng, 1.0f, 10.0f));

    if (intersects(bb, s)) // intersection?
    {
        // aabb of sphere must intersect original aabb
        auto const sbb = aabb_of(s);
        CHECK(intersects(sbb, bb));
    }
    else // no intersection?
    {
        // no aabb point must be contained in the sphere
        for (auto i = 0; i < 100; ++i)
        {
            auto const p = uniform(rng, bb);
            CHECK(!contains(s, p));
        }

        // no sphere point must be contained in the aabb
        for (auto i = 0; i < 100; ++i)
        {
            auto const p = uniform(rng, s);
            CHECK(!contains(bb, p));
        }

        // no point must be contained in both
        for (auto i = 0; i < 100; ++i)
        {
            auto const p = uniform(rng, bb);
            CHECK(!(contains(bb, p) && contains(s, p)));
        }
    }
}

TG_FUZZ_TEST(AABB, TriangleIntersection3)
{
    auto const bounds = tg::aabb3(-10, 10);

    auto const bb = aabb_of(uniform(rng, bounds), uniform(rng, bounds));
    auto const tri = tg::triangle3(uniform(rng, bounds), uniform(rng, bounds), uniform(rng, bounds));

    auto p = uniform(rng, tri);
    if (contains(bb, p))
        CHECK(intersects(bb, tri));

    if (distance(centroid(bb), tri) > distance(centroid(bb), bb.min))
        CHECK(!intersects(bb, tri));

    if (!intersects(bb, tri))
        CHECK(distance(p, bb) > 0);
}

#include <doctest.hh>

#include <ctracer/trace-config.hh>

int main(int argc, char **argv)
{
    doctest::Context context;
    context.applyCommandLine(argc, argv);

    auto res = context.run();

    ct::write_speedscope_json();
    return res;
}

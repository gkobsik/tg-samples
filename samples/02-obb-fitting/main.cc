#include <algorithm>
#include <vector>

#include <typed-geometry/tg.hh>

#include <glow-extras/glfw/GlfwContext.hh>
#include <glow-extras/viewer/view.hh>

namespace
{
tg::rng rng;

struct individual
{
    tg::box3 obb;
    float fitness = 0.0f;
};
bool operator>(individual const& a, individual const& b) { return a.fitness > b.fitness; }

struct environment
{
    std::vector<tg::pos3> pts;
    tg::aabb3 aabb;

    environment()
    {
        for (auto i = 0; i < 100; ++i)
            pts.push_back(uniform(rng, tg::sphere3({0, 0, 0}, 1)));
        for (auto i = 0; i < 100; ++i)
            pts.push_back(uniform(rng, tg::sphere3({2, 0, 0}, 1)));
        // for (auto i = 0; i < 100; ++i)
        //     pts.push_back(uniform(rng, tg::aabb3({-1, 0, -1}, {1, 0, 1})));
        // for (auto i = 0; i < 100; ++i)
        //     pts.push_back(uniform(rng, tg::aabb3({-1, 0, 0}, {1, 1, 1})));

        aabb = aabb_of(pts);
    }

    individual random_individual() const
    {
        auto center = uniform(rng, aabb);
        auto max_size = length(aabb.max - aabb.min);

        auto e0 = tg::uniform<tg::dir3>(rng);
        auto e1 = normalize(cross(e0, tg::uniform<tg::dir3>(rng)));
        auto e2 = normalize(cross(e0, e1));

        auto v0 = e0 * uniform(rng, 0.0f, max_size);
        auto v1 = e1 * uniform(rng, 0.0f, max_size);
        auto v2 = e2 * uniform(rng, 0.0f, max_size);

        individual i;
        i.obb = tg::box3(center, tg::mat3::from_cols(v0, v1, v2));
        i.fitness = fitness_of(i);
        return i;
    }

    individual mutated_individual(individual const& id) const
    {
        auto new_id = id; // copy
        new_id.obb.center += tg::uniform_vec(rng, tg::sphere3::unit) * 0.1f;
        auto rot = tg::rotation_around(tg::uniform<tg::dir3>(rng), uniform(rng, 0_deg, 3_deg));
        for (auto i = 0; i < 3; ++i)
            new_id.obb.half_extents[i] = rot * new_id.obb.half_extents[i] * uniform(rng, 0.9f, 1.1f);
        new_id.fitness = fitness_of(new_id);
        return new_id;
    }

    float fitness_of(individual const& id) const
    {
        float s = 0.0f;
        for (auto const& p : pts)
            if (!contains(id.obb, p))
                s -= 1000.f;

        s -= volume(id.obb);
        // s -= area(id.obb);

        return s;
    }
};
}

int main()
{
    glow::glfw::GlfwContext ctx;

    auto const pop_size = 50;

    environment env;
    std::vector<individual> population;

    for (auto i = 0; i < pop_size; ++i)
        population.push_back(env.random_individual());

    sort(population.begin(), population.end(), std::greater<>());

    std::vector<individual> bests;

    for (auto gen = 0; gen < 2000; ++gen)
    {
        // add new random boxes
        for (auto i = 0; i < pop_size; ++i)
            population.push_back(env.random_individual());

        // add mutated boxes
        for (auto i = 0; i < pop_size; ++i)
            population.push_back(env.mutated_individual(population[i]));

        // sort and let only top ones survive
        sort(population.begin(), population.end(), std::greater<>());
        population.resize(pop_size);

        if (gen % 10 == 0)
            bests.push_back(population.front());
    }
    for (auto const& i : population)
        glow::info() << i.fitness;

    {
        auto v = glow::viewer::grid();
        {
            auto v = gv::view(env.pts);
            for (auto const& id : population)
                gv::view(gv::lines(id.obb), mix(tg::color3::green, tg::color3::red, tg::saturate(-id.fitness / 100.0f)));
        }
        {
            auto v = gv::view(env.pts);
            for (auto i = 0u; i < bests.size(); ++i)
                gv::view(gv::lines(bests[i].obb), mix(tg::color3::green, tg::color3::red, tg::saturate(-bests[i].fitness / 100.0f)));
        }
        {
            auto v = gv::view(env.pts);
            gv::view(gv::lines(population.front().obb));
        }
    }
}

#include <typed-geometry/tg.hh>

#include <glow-extras/glfw/GlfwContext.hh>
#include <glow-extras/viewer/view.hh>

int main()
{
    glow::glfw::GlfwContext ctx;


    // TODO:
    // - sphere
    // - box
    // - cone

    struct prim
    {
        enum
        {
            sphere,
            box,
            cone,
            tube
        } type;

        union data {
            tg::sphere3 sphere;
            tg::box3 box;
            tg::cone3 cone;
            tg::cylinder_boundary_no_caps<3, float> tube;

            data() {}
        } d;

        void show() const
        {
            switch (type)
            {
            case sphere:
                gv::view(d.sphere);
                break;
            case box:
                gv::view(d.box);
                break;
            case cone:
                gv::view(d.cone);
                break;
            case tube:
                gv::view(d.tube);
                break;
            default:
                break;
            }
        }

        tg::optional<float> intersection_parameter(tg::ray3 const& r) const
        {
            switch (type)
            {
            case sphere:
                return tg::closest_intersection_parameter(r, d.sphere);
            case box:
                return tg::closest_intersection_parameter(r, d.box);
            case cone:
                break;
            case tube:
                return tg::closest_intersection_parameter(r, d.tube);
            default:
                break;
            }

            return {};
        }
    };

    tg::rng rng;

    auto aabb = tg::aabb3(-5, 5);

    std::vector<prim> prims;
    auto offs = uniform(rng, 0, 3);
    for (auto i = 0; i < 10; ++i)
    {
        auto& p = prims.emplace_back();

        switch ((i + offs) % 4 * 0 + 3)
        {
        case 0:
            p.type = prim::sphere;
            p.d.sphere = tg::sphere3(uniform(rng, aabb), uniform(rng, 1.0f, 3.0f));
            break;
        case 1:
            p.type = prim::box;
            {
                auto d0 = tg::uniform<tg::dir3>(rng);
                auto d1 = any_normal(d0);
                auto d2 = normalize(cross(d0, d1));
                auto m = tg::mat3();
                m[0] = d0 * uniform(rng, 1.0f, 3.0f);
                m[1] = d1 * uniform(rng, 1.0f, 3.0f);
                m[2] = d2 * uniform(rng, 1.0f, 3.0f);
                p.d.box = tg::box3(uniform(rng, aabb), m);
            }
            break;
        case 2:
            p.type = prim::cone;
            p.d.cone = tg::cone3({uniform(rng, aabb), uniform(rng, 1.0f, 3.0f), tg::uniform<tg::dir3>(rng)}, uniform(rng, 1.0f, 3.0f));
            break;
        case 3:
            p.type = prim::tube;
            p.d.tube = tg::tube3(uniform(rng, aabb), uniform(rng, aabb), uniform(rng, 0.3f, 3.0f));
            break;
        }
    }

    std::vector<tg::pos3> pts;
    std::vector<tg::segment3> rays;
    std::vector<int> pts_prims;

    // trace points against primitives
    {
        tg::ray3 r;
        r.origin = tg::pos3(100);

        for (auto i = 0; i < 1000; ++i)
        {
            r.dir = normalize(uniform(rng, aabb) * 2 - r.origin);

            auto min_t = tg::max<float>();
            auto min_i = -1;
            for (auto pi = 0; pi < int(prims.size()); ++pi)
            {
                auto t = prims[pi].intersection_parameter(r);
                if (t.has_value())
                {
                    min_t = tg::min(t.value(), min_t);
                    min_i = pi;
                }
            }

            if (min_i >= 0)
            {
                pts.push_back(r[min_t]);
                pts_prims.push_back(min_i);
                rays.emplace_back(r.origin, r[min_t]);
            }
        }
    }

    {
        auto v = glow::viewer::view();
        for (auto const& p : prims)
            p.show();
        gv::view(pts, tg::color3::red);
        // view(lines(rays).line_width_world(0.03f), tg::color3::red);
    }

    // TODO: write small raytracer
}
